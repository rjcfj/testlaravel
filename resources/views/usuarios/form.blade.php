<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        <strong>Nome:</strong>
        <input type="text" name="nome" value="{{ $usuario->nome ?? '' }}" class="form-control" placeholder="Nome">
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        <strong>Email:</strong>
        <input type="email" name="email" value="{{ $usuario->email ?? '' }}" class="form-control" placeholder="Email">
    </div>
</div>
